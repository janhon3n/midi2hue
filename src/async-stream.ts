export class AsyncStream<T> {
  private MAX_BUFFER_SIZE = 1000

  private indexGenerator = 0
  private map = new Map<number, T>()

  public async *listen(): AsyncGenerator<T> {
    let index = 0
    while (true) {
      if (index < this.map.size) {
        if (this.map.has(index)) yield this.map.get(index)
        index++
      }

      await new Promise(resolve => setTimeout(resolve, 0))
    }
  }

  public write(value: T) {
    this.map.set(this.indexGenerator, value)

    if (this.map.size > this.MAX_BUFFER_SIZE) {
      this.map.delete(this.indexGenerator - this.MAX_BUFFER_SIZE)
    }

    this.indexGenerator++
  }
}
