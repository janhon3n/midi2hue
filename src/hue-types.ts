export interface Light {
  id: string
  state: {
    on: boolean
    bri: number
    hue: number
    sat: number
  }
  type: string
  name: string
  modelid: string
  uniqueid: string
}

export interface LightGroup {
  id: string
  name: string
  lights: string[]
}
