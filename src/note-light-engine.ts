import { AsyncStream } from './async-stream'
import { MidiEvent, Note, LightState, LightStateChange } from './common-types'

interface Config {
  lightIds: string[]
}

export class NoteLightEngine {
  public stream = new AsyncStream<LightStateChange>()

  private lightStates = new Map<string, LightState>()
  private noteToLightIndex = new Map<string, string>()

  constructor(private config: Config) {}

  public async feed(midiEventStream: AsyncStream<MidiEvent>) {
    for await (let event of midiEventStream.listen()) {
      if (event.type === 'NoteOn') {
        // New note on => get random empty light index and set that light to the state determined by the played note
        this.handleNoteOn(event.note, event.velocity)
      }

      if (event.type === 'NoteOff') {
        this.handleNoteOff(event.note)
      }
    }
  }

  private handleNoteOn(note: Note, velocity: number) {
    const freeLights = []
    for (let id of this.config.lightIds) {
      if (!this.lightStates.has(id)) freeLights.push(id)
    }

    // If no free lights, just ignore the note (for now)
    if (freeLights.length === 0) return

    const randomFreeLight =
      freeLights[Math.floor(Math.random() * freeLights.length)]

    const lightState = this.generateLightStateFromNote(note, velocity)

    this.lightStates.set(randomFreeLight, lightState)
    this.noteToLightIndex.set(note.name, randomFreeLight)

    this.stream.write({
      lightId: randomFreeLight,
      state: lightState,
    })
  }

  private handleNoteOff(note: Note) {
    const lightForThisNote = this.noteToLightIndex.get(note.name)

    if (lightForThisNote) {
      this.lightStates.delete(lightForThisNote)
    }

    this.noteToLightIndex.delete(note.name)

    this.stream.write({
      lightId: lightForThisNote,
      state: null,
    })
  }

  private generateLightStateFromNote(note: Note, velocity: number): LightState {
    const harmonyColor = (note.key % 2 == 0 ? note.key : note.key - 6) / 12
    return {
      hue: harmonyColor,
      brightness: velocity,
      saturation: 1.0,
    }
  }
}
