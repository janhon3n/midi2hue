import axios from 'axios'
import { Light, LightGroup } from './hue-types'
import { LightState, LightStateChange } from './common-types'
import { AsyncStream } from './async-stream'

interface Config {
  bridgeIp: string
  username: string
}

export class HueService {
  private baseUrl: string
  public groups: LightGroup[]
  public lightIds: string[] = []

  constructor(private config: Config) {
    this.baseUrl = `http://${config.bridgeIp}/api/${config.username}`
  }

  public async initialize(): Promise<void> {
    const { data } = await axios.get(`${this.baseUrl}/groups`)
    this.groups = Object.keys(data).map(key => ({
      ...data[key],
      id: key,
    }))
  }

  public async selectGroup(groupId: string) {
    const selectedGroup = this.groups.find(g => g.id === groupId)
    this.lightIds = selectedGroup.lights
  }

  public async feed(lightStateChangeStream: AsyncStream<LightStateChange>) {
    for await (let change of lightStateChangeStream.listen()) {
      if (change.state) {
        this.turnOnLight(change.lightId, change.state)
      } else {
        this.turnOffLight(change.lightId)
      }
    }
  }

  public async turnOnLight(lightId: string, state: LightState) {
    const res = await axios.put(`${this.baseUrl}/lights/${lightId}/state`, {
      on: true,
      hue: Math.floor(state.hue * 65535),
      sat: Math.floor(state.saturation * 255),
      bri: Math.floor(state.brightness * 255),
      transitiontime: 0,
    })
  }

  public async turnOffLight(lightId: string) {
    const res = await axios.put(`${this.baseUrl}/lights/${lightId}/state`, {
      on: false,
      transitiontime: 0,
    })
  }
}
