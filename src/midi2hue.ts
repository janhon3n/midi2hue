import { NodeMidiRawInput } from './midi-raw-input'

import term from 'terminal-kit'
import { MidiEventTransformer } from './midi-event-transformer'
import { HueService } from './hue-service'
import { NoteLightEngine } from './note-light-engine'
const terminal = term.terminal

interface Config {
  bridgeIp: string
  username: string
}

export class Midi2Hue {
  private shutdown: boolean = false

  private rawMidiInput: NodeMidiRawInput
  private midiEventTransformer: MidiEventTransformer
  private noteLightEngine: NoteLightEngine
  private hueService: HueService

  constructor(private config: Config) {}

  public async main() {
    // Setup hue
    this.hueService = new HueService(this.config)
    await this.hueService.initialize()
    await this.selectHueGroup()

    // Setup midi stream
    this.rawMidiInput = new NodeMidiRawInput()
    await this.selectMidiDevice()

    this.midiEventTransformer = new MidiEventTransformer()
    this.midiEventTransformer.feed(this.rawMidiInput.stream)

    this.noteLightEngine = new NoteLightEngine({
      lightIds: this.hueService.lightIds,
    })

    this.noteLightEngine.feed(this.midiEventTransformer.stream)
    this.hueService.feed(this.noteLightEngine.stream)

    await this.showRuntimeMenu()
  }

  public async close() {
    this.rawMidiInput.close()
    this.shutdown = true
    process.exit()
  }

  /* UI Stuff */
  public async selectHueGroup() {
    console.info()
    terminal.white('Select hue light group:')
    const selected = (await new Promise((resolve, reject) =>
      terminal.singleColumnMenu(
        this.hueService.groups.map(group => group.name),
        {
          style: terminal.green,
        },
        (err, res) => {
          if (err) reject(err)
          else resolve(res)
        }
      )
    )) as any
    this.hueService.selectGroup(
      this.hueService.groups[selected.selectedIndex].id
    )
    console.info()
  }

  public async selectMidiDevice() {
    const inputDevices = this.rawMidiInput.listDevices()

    console.info()
    terminal.white('Select midi input device:')
    const selected = (await new Promise((resolve, reject) =>
      terminal.singleColumnMenu(
        inputDevices,
        {
          style: terminal.green,
        },
        (err, res) => {
          if (err) reject(err)
          else resolve(res)
        }
      )
    )) as any

    console.info()
    this.rawMidiInput.selectDevice(selected.selectedIndex)
  }

  public async showRuntimeMenu() {
    terminal.singleColumnMenu(['Kill me now'], () => {
      this.close()
    })
  }
}
