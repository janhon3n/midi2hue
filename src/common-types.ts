export interface Note {
  octave: number
  key: number
  name: string
}

interface NoteOn {
  type: 'NoteOn'
  note: Note
  velocity: number
}

interface NoteOff {
  type: 'NoteOff'
  note: Note
  velocity: number
}

export type MidiEvent = NoteOn | NoteOff

export interface LightState {
  // All values between [0,1]
  hue: number
  saturation: number
  brightness: number
}

export interface LightStateChange {
  lightId: string
  state: LightState
}
