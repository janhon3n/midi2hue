require('dotenv').config()

import { Midi2Hue } from './midi2hue'

let midi2hue: Midi2Hue

midi2hue = new Midi2Hue({
  bridgeIp: process.env['HUE_BRIDGE_IP'],
  username: process.env['HUE_USERNAME'],
})

midi2hue.main()
