import midi from 'midi'
import { AsyncStream } from './async-stream'

export interface RawMidiInput {
  stream: AsyncStream<number[]>
  listen: () => AsyncGenerator<number[]>
  close: () => Promise<void>
}

export class NodeMidiRawInput implements RawMidiInput {
  private input
  public stream = new AsyncStream<number[]>()

  constructor() {
    this.input = new midi.Input()
    this.input.on('message', (deltatime, message) => {
      this.stream.write(message)
    })
  }

  public listDevices(): string[] {
    const inputs = []
    for (let i = 0; i < this.input.getPortCount(); i++) {
      inputs.push(this.input.getPortName(i))
    }
    return inputs
  }

  public selectDevice(number: number) {
    this.input.openPort(number)
  }

  public listen(): AsyncGenerator<number[]> {
    return this.stream.listen()
  }

  public async close(): Promise<void> {
    this.input.closePort()
  }
}
