import { AsyncStream } from './async-stream'
import { MidiEvent, Note } from './common-types'

export class MidiEventTransformer {
  public stream = new AsyncStream<MidiEvent>()

  constructor() {}

  public async feed(inputStream: AsyncStream<number[]>) {
    for await (const input of inputStream.listen()) {
      const [status, arg1, arg2] = input

      // Transform byte data into js objects
      let event: MidiEvent
      if (status >= 0x90 && status < 0xa0) {
        event = {
          type: 'NoteOn',
          note: this.convertToNote(arg1),
          velocity: arg2 / 128,
        }
      } else if (status >= 0x80 && status < 0x90) {
        event = {
          type: 'NoteOff',
          note: this.convertToNote(arg1),
          velocity: arg2 / 128,
        }
      } else {
        console.warn('Unknown midi input data:', status, arg1, arg2)
      }

      this.stream.write(event)
    }
  }

  private keyNames = [
    'C',
    'C#',
    'D',
    'D#',
    'E',
    'F',
    'F#',
    'G',
    'G#',
    'A',
    'A#',
    'B',
  ]
  private convertToNote(byte: number): Note {
    const octave = Math.floor(byte / 12)
    const key = byte % 12
    const name = this.keyNames[key] + octave
    return {
      key,
      name,
      octave,
    }
  }
}
